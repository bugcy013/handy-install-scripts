#!/bin/bash
#
#   This script installs eAccelerator.
#   
#   Copyright (C) 2013 Craig Parker <craig@paragon.net.uk>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3 of the License, or
#   (at your option) any later version.
#   
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; If not, see <http://www.gnu.org/licenses/>.
#
#

cd /usr/src;

rm -f master*;

rm -rf eaccelerator*;

wget https://github.com/eaccelerator/eaccelerator/tarball/master;

tar -xvf master;

cd eaccelerator*;

phpize;

./configure && make && make install;

echo -e 'extension="eaccelerator.so"\neaccelerator.shm_size="16"\neaccelerator.cache_dir="/tmp/eaccelerator"\neaccelerator.enable="1"\neaccelerator.optimizer="1"\neaccelerator.check_mtime="1"\neaccelerator.debug="0"\neaccelerator.filter=""\neaccelerator.shm_ttl="0"\neaccelerator.shm_prune_period="0"\neaccelerator.shm_only="0"' >> /usr/local/lib/php.ini;

mkdir -p /tmp/eaccelerator;

chmod 0777 /tmp/eaccelerator;

echo "Restarting httpd";

service httpd restart;

echo "Checking php info."

php -i | grep eaccelerator;


