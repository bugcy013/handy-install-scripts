#!/bin/bash
#
#   This script installs APC and sets the cache size depending on the total
#   RAM size of the server.
#   
#   Copyright (C) 2013 Craig Parker <craig@paragon.net.uk>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3 of the License, or
#   (at your option) any later version.
#   
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; If not, see <http://www.gnu.org/licenses/>.
#
#

RAM=`free -m | grep Mem | awk '{print $2}'`

printf "\n" | pecl install apc

if [ $RAM -le 1024 ]; then
        
         echo -e "extension = apc.so \napc.enabled = 1 \napc.shm_size = 128M" >> /usr/local/lib/php.ini;

    elif [ $RAM -le 4096 ]; then
                
         echo -e "extension = apc.so \napc.enabled = 1 \napc.shm_size = 256M" >> /usr/local/lib/php.ini;

    else 

         echo -e "extension = apc.so \napc.enabled = 1 \napc.shm_size = 512M" >> /usr/local/lib/php.ini;

fi 


service httpd restart