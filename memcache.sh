#!/bin/bash
#
#   This script installs memcache for cPanel on CentOS
#      
#   Copyright (C) 2013 Craig Parker <craig@paragon.net.uk>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3 of the License, or
#   (at your option) any later version.
#   
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; If not, see <http://www.gnu.org/licenses/>.
#
#

# Install some dependencies.
yum -y install libevent libevent-devel

# Download
cd /usr/src
wget http://memcached.googlecode.com/files/memcached-1.4.15.tar.gz

# Extract and change to directory.

tar -xzf memcached-1.4.15.tar.gz
cd memcached-1.4.15

# Configure and install.

./configure && make && make install

# Install the init Script

wget http://imadinosaur.net/scripts/memcacheinit

mv memcacheinit /etc/init.d/memcached

chmod 755 /etc/init.d/memcached

chkconfig --add memcached

/etc/init.d/memcached start

# Install the module.

printf "\n" | pecl install memcache

# Add the module to the php.ini

echo -e "extension = memcache.so" >> /usr/local/lib/php.ini;

# Restart httpd

service httpd restart