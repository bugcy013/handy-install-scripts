#!/bin/bash
#
#   This script assumes that you are running CentOS and that you want to install 
#   Varnish on either the shared IP (if cPanel) or the single server IP.
#   
#   Copyright (C) 2013 Craig Parker <craig@paragon.net.uk>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3 of the License, or
#   (at your option) any later version.
#   
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; If not, see <http://www.gnu.org/licenses/>.
#
#

if [ -d /usr/local/cpanel ] ; then

		export IP=`grep ADDR /etc/wwwacct.conf | awk '{print $2}'`

	else

		export IP=`ifconfig | grep "inet addr" | grep -v "127.0.0.1" | awk '{print $2}' | awk -F ":" '{print $2}' | head -n 1`

fi


rpm --nosignature -i http://repo.varnish-cache.org/redhat/varnish-3.0/el5/noarch/varnish-release-3.0-1.noarch.rpm;

yum -y install varnish;

sed -i "s/VARNISH_LISTEN_PORT=6081/VARNISH_LISTEN_PORT=80/" /etc/sysconfig/varnish;

sed -i "s/127.0.0.1/$IP/" /etc/varnish/default.vcl;

sed -i "s/80/8080/" /etc/varnish/default.vcl;


if [ -d /usr/local/cpanel ] ; then

		sed -i "s/80/8080/" /var/cpanel/cpanel.config;

		/usr/local/cpanel/whostmgr/bin/whostmgr2 --updatetweaksettings;

		service httpd restart;

		sleep 10;

		/etc/init.d/varnish restart;

		echo "Finished :)"

	else

		sed -i "s/Listen $IP:80/Listen $IP:8080/" /etc/httpd/conf/httpd.conf;

		service httpd restart;

		sleep 10;

		/etc/init.d/varnish restart

		echo "Finished :)"

fi



