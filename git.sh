#!/bin/bash
#
#   This script installs the git client on CentOS
#   
#   Copyright (C) 2013 Craig Parker <craig@paragon.net.uk>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3 of the License, or
#   (at your option) any later version.
#   
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; If not, see <http://www.gnu.org/licenses/>.
#
#

yum -y install zlib-devel openssl-devel cpio expat-devel gettext-devel

# Download and extract

cd /usr/src

wget https://git-core.googlecode.com/files/git-1.8.2.tar.gz

tar -xvf git-1.8.2.tar.gz

cd git-1.8.2

# Compile and Install

./configure && make && make install